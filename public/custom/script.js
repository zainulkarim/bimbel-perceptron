jQuery.expr.filters.offscreen = function(el) {
  var rect = el.getBoundingClientRect();
  return (
           (rect.x + rect.width) < 0 
             || (rect.y + rect.height) < 0
             || (rect.x > window.innerWidth || rect.y > window.innerHeight)
         );
};
function visimisi(){
	$('html, body').animate({
	    scrollTop: $("#visimisi_id").offset().top
	}, 700);
}
function bimbel(){
	$('html, body').animate({
	    scrollTop: $("#bimbel_id").offset().top
	}, 700);
}
function bantuan(){
	$('html, body').animate({
	    scrollTop: $("#bantuan_id").offset().top
	}, 700);
}
function beranda(){
	$('html, body').animate({
	    scrollTop: $("#beranda_id").offset().top
	}, 700);
}
$(document).scroll(function(){
	if($('#beranda_id').is(':offscreen')){
		$('#beranda_point').attr("class","fa fa-circle-o pointer")
	}
	else{
		$('#beranda_point').attr("class","fa fa-circle pointer")	
	}

	if($('#visimisi_id').is(':offscreen')){
		$('#visimisi_point').attr("class","fa fa-circle-o pointer")
	}
	else{
		$('#visimisi_point').attr("class","fa fa-circle pointer")	
	}

	if($('#bimbel_id').is(':offscreen')){
		$('#bimbel_point').attr("class","fa fa-circle-o pointer")
	}
	else{
		$('#bimbel_point').attr("class","fa fa-circle pointer")	
	}

	if($('#bantuan_id').is(':offscreen')){
		$('#bantuan_point').attr("class","fa fa-circle-o pointer")
	}
	else{
		$('#bantuan_point').attr("class","fa fa-circle pointer")	
	}
})
function parse(){
	$('#parse_id').click()
}