<!DOCTYPE html>
<html>
	<head>
		<title>Selamat Datang</title>
		<link rel="stylesheet" type="text/css" href="public/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="public/custom/style.css">
		<link rel="stylesheet" type="text/css" href="public/fontawesome/css/font-awesome.min.css">
		<script type="text/javascript" src="public/js/jquery.js"></script>
		<script type="text/javascript" src="public/custom/script.js"></script>
	</head>
	<body>
		<div class="index-page" id="beranda_id">	
			<div class="header-page">
				<div class="col-sm-6">
					<h3 class="title-page">MTs Al-Huda Gondang</h3>
					<span class='subtitle-page'>MTs Terbaik di Gondang Nganjuk</span>
				</div>
				<div class="col-sm-6 search-bar">
					<input type="text" class="search" placeholder="Search..." style="display:none;">
					<button class="slide-button" style="margin-top:10px;">Upload Hasil Bimbel</button>
				</div>
			</div>
			<div class="top-nav">
				<ul class="ul">
					<li class="li"><a class="active" href="#">Beranda</a></li>
					<li class="li"><a href="#" onclick="visimisi()">Visi Misi</a></li>
					<li class="li"><a href="#" onclick="bimbel()">Bimbingan Belajar</a></li>
					<li class="li"><a href="#">Bantuan</a></li>
				</ul>
			</div>
			<div class="slide">
				<div class="floating-tab">
					<span class="slide-title">
						PESERTA BIMBINGAN
					</span>
					<br><br>
					<span class="slide-subtitle">
						Menentukan peserta bimbingan belajar berdasarkan rekam jejak nilai
					</span>
					<br><br>
					<button class="slide-button" onclick="help()">Pelajari</button>
				</div>
			</div>
		</div>
		<div class="visimisi-page row" id="visimisi_id">
			<div class="visimisi-bg col-sm"></div>
			<div class="visimisi-content col-sm">
			<h2 align="center">
				Visi
			</h2>
				Membentuk generasi yang cerdas, berilmu amaliah
				dan beramal ilmiah
			<br><br>
			<h2 align="center">
				Misi
			</h2>
				<ul style="text-align:left">
					<li>
					Melaksanakan pembelajaran yang efektif,efisien, dan komprehensif ( IQ, EQ,SQ )
					</li>
					<li>
					Menyelenggarakan pembelajaran yang seimbang dan terpadu yang meliputi ilmu Agama ilmu umum dan teknologi
					</li><li>
					Menumbuhkembangkan pola fikir dan pola perilaku yang islami ( Akhlakul Karimah )
					</li><li>
					Menumbuhkembangkan partisipasi seluruh warga sekolah dan masyarakat dalam meningkatkan mutu pendidikan
					</li><li>
					Mempersiapkan siswa dalam melanjutkan ke jenjang pendidikan yang lebih tinggi.	
					</li>
				</ul>
			</div>
		</div>
		<div class="bimbel-page row" id="bimbel_id">
			<form method="post" enctype="multipart/form-data" action="#bimbel_id">
			<input type="submit" style="display:none" id="parse_id" />
			<label class="slide-button">
				Upload XLS
				<input type="file" style="display : none" name="file" onchange="parse()">
			</label>
			</form>
			<?php
			if (isset($_FILES['file'])) {
				require_once 'public/excelparser/simplexlsx.class.php';
				if ( $xlsx = SimpleXLSX::parse( $_FILES['file']['tmp_name'] ) ) {
				echo'<table border="1" cellpadding="3" style="border-collapse: collapse;">';
					list( $cols, ) = $xlsx->dimension();
					foreach ( $xlsx->rows() as $k => $r ) {
						echo '<tr>';
						for ( $i = 0; $i < $cols; $i ++ ) {
							echo '<td><input type="text" value="' . ( ( isset( $r[ $i ] ) ) ? $r[ $i ] : '&nbsp;' ) . '" style="border:none;"></td>';
						}
						echo '</tr>';
					}
					echo '</table>';
				} else {
					echo SimpleXLSX::parse_error();
				}
			}
			?>
		</div>
		<div class="bantuan-page row" id="bantuan_id">
			<div class="bantuan-content col-sm">
				Ngene Bos..<br>
				<p>
					Iki awakmu kudu ngisi eksel disek, lha terus engko metune yo excel misan, ngono tok wes
				</p>
			</div>
			<div class="visimisi-bg col-sm"></div>
		</div>

		<div class="float-menu">
			<span class='fa fa-circle-o pointer' title="Beranda" id='beranda_point'></span>
			<span class='fa fa-circle-o pointer' title="Visi Misi" id='visimisi_point'></span>
			<span class='fa fa-circle-o pointer' title="Bimbingan Belajar" id='bimbel_point'></span>
			<span class='fa fa-circle-o pointer' title="Bantuan" id='bantuan_point'></span>
		</div>
	</body>
</html>